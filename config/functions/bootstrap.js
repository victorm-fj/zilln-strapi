'use strict';

/**
 * An asynchronous bootstrap function that runs before
 * your application gets started.
 *
 * This gives you an opportunity to set up your data model,
 * run jobs, or perform some special logic.
 */

module.exports = cb => {
  const io = require('socket.io')(strapi.server);
  // listen for user connection
  io.on('connection', function(socket) {
    // send message on user connection
    socket.emit(
      'hello',
      JSON.stringify({ message: 'Hello from Zill strapi app' })
    );
  });
  // listen for user disconnect
  socket.on('disconnect', () => console.log('a user has disconnected'));
  // register socket io inside strapi main object to use it globally anywhere
  strapi.io = io;
  // define emit to all users Fns
  strapi.emitChallengeChange = () =>
    io.emit(
      'challenge_change',
      JSON.stringify({ message: 'There has been a change in challenges' })
    );
  strapi.emitProductChange = () =>
    io.emit(
      'product_change',
      JSON.stringify({ message: 'There has been a change in products' })
    );
  cb();
};
