FROM node:8

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install --production

COPY . .

EXPOSE 1337

CMD ["npm", "run", "start:production"]
